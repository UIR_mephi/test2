import logging


class ObjectInfo():  #запоминает место ошибки при парсинге
    logging.debug(u'Зашли в класс ObjectInfo')
    def __init__(self, start_mark):
        self.line = start_mark.line + 1
        self.file = start_mark.name

    def __str__(self):
        return "%s:%s" % (self.file, self.line)

    def __repr__(self):
        return self.__str__()
from Env import Env
import logging

logger = logging.getLogger("Cls")


def register(cls):      #это декоратор????
    logger.info(u'Здесь выводится класс')
    print("reg %s" % cls)       #а что выводится из этого класса
    Env().reg_class(cls)
    return cls


@register
class Module:
    logger.debug(u'Зашли в класс Module')
    yaml_tag = u"!module"

    def __init__(self, *args, **kwargs):
        self.node = kwargs['node'] if 'node' in kwargs else {}
        self.info = kwargs['info'] if 'info' in kwargs else {}
        logger.warning(u'ПЕЧАТАЕМ УЗЕЛ')
        print(self.node)
        self.env = Env()
        try:
            self.name = self.node["name"]
        except KeyError:
            raise Exception("no name %s"%(self.info))
        self.env.reg_module(self.name, self)
        self.interfaces_release = self.node['interfaces_release'] if 'interfaces_release' in self.node else []
        self.interfaces_use = self.node['interfaces_use'] if 'interfaces_use' in self.node else []
        pass

    def interface_release_itr(self): #класс итератор
        for interface in self.interfaces_release:
            logger.info( u'Здесь должен быть вывод генератора' )
            logger.info("yield cls %s", interface)
            yield interface

    def interface_use_itr(self): #класс итератор
        for interface in self.interfaces_use:
            logger.info( u'Здесь должен быть вывод генератора' )
            logger.info("yield cls %s", interface)
            yield interface

    def __str__(self):
        return self


@register
class Interface:
    logger.debug(u'Зашли в класс Interface')
    yaml_tag = u"!interface"

    def __init__(self, *args, **kwargs):
        self.node = kwargs['node'] if 'node' in kwargs else {}
        self.info = kwargs['info'] if 'info' in kwargs else {}
        print(self.node)
        try:
            self.name = self.node["name"]
        except KeyError:
            raise Exception("no name %s"%(self.info))
        self.env = Env()
        self.env.reg_interface(self.name, self)
        logger.debug(u'Выводим названия интерфейсов')


    def __str__(self):
        return self

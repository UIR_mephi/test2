import logging
logger = logging.getLogger("Env")

def Singleton(cls):     #определение декоратора Singeton: определяет есть ли класс в экзмплярах и если нет добавляет
    instances = {}      #примеры, экземпляры в словаре

    # зачем мы делаем отдельную функцию обертку, потому что декоратор? тип если убрать эту ф-ию работать не будет
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


@Singleton      #декоратор -> оборачивает следующую за ним функцию
class Env: #окружение
    logger.debug(u'Зашли в класс Env')

    def __init__(self):
        self.classes = {}       #словарь классов?
        self.modules = {}
        self.interfaces = {}

    def reg_class(self, cls):
        self.classes[cls.yaml_tag] = cls

    def classes_itr(self): #класс итератор
        for cls in self.classes.values():
            logger.info( u'Здесь должен быть вывод генератора' )
            print("yield cls %s", cls)
            yield cls
        #Yield- ключевое слово, исп как return, но вернет генератоор (список)

    def get_class_by_tag(self, tag):
        return self.classes[tag]

    def reg_module(self, name, module):
        self.modules[name] = module

    def modules_itr(self): #класс итератор
        for mod in self.modules.values():
            logger.info( u'Здесь должен быть вывод генератора' )
            logger.info("yield cls %s", mod)
            yield mod

    def get_module_by_name(self, name):
        return self.modules[name]

    def reg_interface(self, name, ifc):
        self.interfaces[name] = ifc

    def interfaces_itr(self): #класс итератор
        for interface in self.interfaces.values():
            logger.info( u'Здесь должен быть вывод генератора' )
            logger.info("yield cls %s", interface)
            yield interface

    def get_interface_by_name(self, name):
        return self.interfaces[name]

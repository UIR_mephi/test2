import yaml
from collections import OrderedDict
from Object import ObjectInfo
from Env import Env
import logging
import Module_Interface

logger = logging.getLogger("Load")

logger.debug( u'Начали код Load' )


def load_include(f):
    logger.debug(u'Зашли в функцию load_include')
    yaml.load(f, SpectraLoader)


class MyLoader(yaml.Loader):
    logger.debug(u'Зашли в класс MyLoader')
    def __init__(self, stream, *args, **kwargs):
        logger.debug(u'начали инициализирование MyLoader')
        yaml.Loader.__init__(self, stream, *args, **kwargs)

        self.env = Env()

        self.constructed_objects_info = {}
        self.constructed_objects = {}
        self.deep_construct = []

        self.add_constructor(u'tag:yaml.org,2002:map', type(self).construct_mapping)
        self.add_constructor(u'tag:yaml.org,2002:omap', type(self).construct_mapping)
        self.add_constructor(u'tag:yaml.org,2002:seq',type(self).construct_sequence)

        for cls in self.env.classes_itr():
            funct = self.obj_constructor(cls.yaml_tag, self.env)
            self.add_constructor(cls.yaml_tag, funct)
        logger.debug(u'закончили иницилизирование MeLoader')

    def obj_constructor(self, tag, env):
        #logger.debug(u'Зашли в функцию obj_constructor')
        cls = self.env.get_class_by_tag(tag)

        def object_constructor(self, node):
            obj_info = ObjectInfo(node.start_mark)
            try:
                data, data_info = self.construct_mapping(node, deep=True)
            except Exception as e:
                raise

            try:
                print("construct cls")
                obj = cls(node = data, info = data_info, loader = load_include)
            except Exception as e:
                raise

            return obj, obj_info

        return object_constructor

    def get_object_constructor(self, node):
        #logger.debug(u'Зашли в функцию get_object_constructor')
        constructor = None
        tag_suffix = None
        if node.tag in self.yaml_constructors:
            constructor = self.yaml_constructors[node.tag]
        else:
            for tag_prefix in self.yaml_multi_constructors:
                if node.tag.startswith(tag_prefix):
                    tag_suffix = node.tag[len(tag_prefix):]
                    constructor = self.yaml_multi_constructors[tag_prefix]
                    break
            else:
                if None in self.yaml_multi_constructors:
                    tag_suffix = node.tag
                    constructor = self.yaml_multi_constructors[None]
                elif None in self.yaml_constructors:
                    constructor = self.yaml_constructors[None]
                elif isinstance(node, yaml.ScalarNode):
                    constructor = self.__class__.construct_scalar
                elif isinstance(node, yaml.SequenceNode):
                    constructor = self.construct_sequence
                elif isinstance(node, yaml.MappingNode):
                    constructor = self.construct_mapping
        return (constructor, tag_suffix)

    def construct_object(self, node, deep=False):
        #logger.debug(u'Зашли в функцию constructor_object')
        # return already constructed object
        self.deep_construct.append(deep)
        if node in self.recursive_objects:
            raise yaml.constructor.ConstructorError(None, None,
                    "found unconstructable recursive node", node.start_mark)
        self.recursive_objects[node] = None

        constructor = None
        tag_suffix = None
        constructor, tag_suffix = self.get_object_constructor(node)

        if tag_suffix is None:
            if isinstance(node, yaml.SequenceNode):
                data, data_info = constructor(self, node)
            elif isinstance(node, yaml.MappingNode):
                data, data_info = constructor(self, node)
            else:
                data = constructor(self, node)
                data_info = ObjectInfo(node.start_mark)
        else:
            data, data_info = constructor(self, tag_suffix, node)

        del self.recursive_objects[node]
        self.deep_construct.pop()

        return data, data_info

    def construct_sequence(self, node, deep=False):
        #logger.debug(u'Зашли в функцию constructor_sequence')
        if not isinstance(node, yaml.SequenceNode):
            raise yaml.constructor.ConstructorError(None, None,
                    "expected a sequence node, but found %s" % node.id,
                    node.start_mark)
        data = []
        data_info = []
        for child in node.value:
            result = self.construct_object(child, deep=deep)
            data.append(result[0])
            data_info.append(result[1])
        return data, data_info

    def construct_mapping(self, node, deep=False):
        #logger.debug(u'Зашли в функцию constructor_mapping')
        if isinstance(node, yaml.MappingNode):
            self.flatten_mapping(node)
        else:
            raise yaml.constructor.ConstructorError(None, None,
                'expected a mapping node, but found %s' % node.id, node.start_mark)
        mapping = OrderedDict()     #подкласс dict - запоминает порядок добавления записей
        mapping_info = {}

        for key_node, value_node in node.value:
            key, key_info = self.construct_object(key_node, deep=deep)
            try:
                hash(key)
            except TypeError as exc:
                raise yaml.constructor.ConstructorError('while constructing a mapping',
                    node.start_mark, 'found unacceptable key (%s)' % exc, key_node.start_mark)
            value, value_info = self.construct_object(value_node, deep=deep)
            mapping[key] = value
            mapping_info[key] = (key_info, value_info)
        return mapping, mapping_info


with open('test.yaml') as f:
    #logger.debug(u'Пытаемся загрузить данные из файла yaml')
    data = yaml.load(f, MyLoader)

import yaml
from collections import OrderedDict
from Object import ObjectInfo
from Env import Env
import logging
import Module_Interface
from Load import MyLoader

import argparse

parser = argparse.ArgumentParser(description='interface')
parser.add_argument('--ifc', type=str)
args = parser.parse_args()
search_interface = args.ifc

logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level = logging.DEBUG, filename = "test.log")

with open('test.yaml') as f:
    #logging.debug(u'Пытаемся загрузить данные из файла yaml')
    data = yaml.load(f, MyLoader)


print("вывод всех модулей")
env = Env()

uses = []
releases = []
for mod in env.modules_itr():
    for ifc_release in mod.interface_release_itr():
        if ifc_release.name == search_interface:
            releases.append(mod.name)
    for ifc_use in mod.interface_use_itr():
        if ifc_use.name == search_interface:
            uses.append(mod.name)

print("Модули, использующие %s интерфейс: %s" % (search_interface, uses))
print("Модули, реализующие %s интерфейс: %s" % (search_interface, releases))

print("10")

